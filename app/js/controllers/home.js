function HomeCtrl(AppSettings, FACE) {
  'ngInject';
  
  var vm = this;
  vm.reports = [];

  const commands = [
  	{action:'PLACE', x:0, y:0, f:'NORTH'},
  	{action:'MOVE'},
  	{action:'MOVE'},
  	{action:'REPORT'},

  	{action:'RIGHT'},
  	{action:'MOVE'},
  	{action:'REPORT'},

  	{action:'PLACE', x:4, y:3, f:'EAST'},
  	{action:'MOVE'},
  	{action:'REPORT'},

  	{action:'LEFT'},
  	{action:'MOVE'},
  	{action:'LEFT'},
  	{action:'MOVE'},
  	{action:'REPORT'},

  	{action:'LEFT'},
  	{action:'MOVE'},
  	{action:'LEFT'},
  	{action:'MOVE'},
  	{action:'REPORT'}
  ];

  vm.executeCommand = function(status, command) {  	
  	if (status == {} && command.action.toUpperCase() != 'PLACE') {
  		console.log ('you need to place the robot in a proper place');
  		return;
  	}

	let newStatus = Object.assign({}, status);

	switch (command.action.toUpperCase()){
	  	case 'PLACE':
	  		newStatus = {
	  			x: command.x,
	  			y: command.y,
	  			nf: FACE[command.f]		// replace FACE with number for clean implementation
	  		}
		  	break;
		case 'MOVE':
			switch (status.nf){
				case 0:
					newStatus.y++;
					break;
				case 1:
					newStatus.x--;
					break;
				case 2:
					newStatus.y--;
					break;				
				case 3:
					newStatus.x++;
					break;
				default:
					break;
			}
			
		  	break;
		case 'LEFT':
			newStatus.nf = (newStatus.nf - 1 + 4) % 4;
		  	break;
		case 'RIGHT':
			newStatus.nf = (newStatus.nf + 1) % 4;
		  	break;
		case 'REPORT':
		  	vm.reports.push({
		  		x: status.x,
		  		y: status.y,
		  		f: readableFACE(status.nf)
		  	});

		  	break;
		default:
		  	break;
	}

	if (isStatusValid(newStatus))
		return newStatus;

	return status;
  }

  function isStatusValid(status) {
	  if (status.x < 0 ||
		  	status.x >= AppSettings.cols ||
		  	status.y < 0 ||
		  	status.y >= AppSettings.rows) {
	  	console.log ('This status is invalid', status);
	  	return false;
	  }

	  return true;
  }

  function readableFACE(nf) {
  	for (let f in FACE) {
  		if (FACE[f] == nf) {
  			return f;
  		}
  	}
  }

  commands.reduce(function(status, command){
	return vm.executeCommand(status, command);
  }, {});	  
}

export default {
  name: 'HomeCtrl',
  fn: HomeCtrl
};