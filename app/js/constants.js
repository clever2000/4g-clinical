export const AppSettings = {
  appTitle: 'Robot Application',
  rows: 5,
  cols: 5
};

export const FACE = {
	'NORTH': 0,
	'EAST': 1,
	'SOUTH': 2,
	'WEST': 3
};
