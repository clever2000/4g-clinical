'use strict';

describe('Unit: HomeCtrl', function() {

  let ctrl;

  beforeEach(function() {
    // instantiate the app module
    angular.mock.module('app');

    angular.mock.inject(($controller) => {
      ctrl = $controller('HomeCtrl');
    });
  });

  it('should exist', function() {
    expect(ctrl).toBeDefined();    
  });

  it('ignores all other commands except "place" until robot is properly placed on the table.', function() {
    expect(ctrl.executeCommand({}, {action:'MOVE'}))
      .toEqual({});
  });

  it('ignores an invalid "place" action', function() {    
    expect(ctrl.executeCommand({x:3, y:3, nf:1}, {action:'PLACE', x:-1, y:2, f:'NORTH'}))
      .toEqual({x:3, y:3, nf:1});
  });

  it('doesn\'t move to an invalid position', function() {
    expect(ctrl.executeCommand({x:4, y:4, nf:0}, {action:'MOVE'}))
      .toEqual({x:4, y:4, nf:0});
  });

  it('moves successfully', function() {
    expect(ctrl.executeCommand({x:4, y:4, nf:2}, {action:'MOVE'}))
      .toEqual({x:4, y:3, nf:2});
    expect(ctrl.executeCommand({x:2, y:2, nf:3}, {action:'MOVE'}))
      .toEqual({x:3, y:2, nf:3});
  });

  it('turns successfully', function() {
    expect(ctrl.executeCommand({x:4, y:4, nf:0}, {action:'LEFT'}))
      .toEqual({x:4, y:4, nf:3});
    expect(ctrl.executeCommand({x:4, y:4, nf:1}, {action:'RIGHT'}))
      .toEqual({x:4, y:4, nf:2});
  });
});